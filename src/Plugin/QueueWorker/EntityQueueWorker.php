<?php

namespace Drupal\convert_entity_media_embed\Plugin\QueueWorker;

use Drupal\convert_entity_media_embed\FieldTypes;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Finds entities with values for a field and builds a queue for them.
 *
 * @QueueWorker(
 *   id = "convert_entity_media_embed_entities",
 *   title = @Translation("Find entities for fields."),
 *   cron = {"time" = 60}
 * )
 *
 */
class EntityQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entityTypeId = $data['entity_type'];
    $entityId = $data['id'];
    $replacementPerformed = FALSE;

    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->load($entityId);

    if (empty($entity)) {
      return;
    }

    $fieldDefinitions = $entity->getFieldDefinitions();

    foreach ($entity->getTranslationLanguages() as $langcode => $language) {
      $translation = $entity->getTranslation($langcode);

      foreach ($fieldDefinitions as $fieldDefinition) {
        if (!in_array($fieldDefinition->getType(), FieldTypes::TYPES)) {
          continue;
        }

        $fieldItemList = $translation->get($fieldDefinition->getName());
        if ($fieldItemList->isEmpty()) {
          continue;
        }

        for ($i = 0; $i < $fieldItemList->count(); $i++) {
          $item = $fieldItemList->get($i);
          $value = $item->getValue();
          $originalValue = $value['value'];
          $value['value'] = preg_replace_callback(
            '/<drupal-entity(.*)data-entity-type="media"(.*)>(&nbsp;)?<\/drupal-entity>/',
            [static::class, 'getDrupalMediaTag'],
            $originalValue
          );

          if ($value['value'] != $originalValue) {
            $item->setValue($value);
            $fieldItemList->set($i, $item);
            $replacementPerformed = TRUE;
          }
        }
      }
    }

    if ($replacementPerformed) {
      $entity->save();
    }
  }

  /**
   * Callback to get new drupal-media tag based on matched values.
   *
   * @param array $matches
   *   A list of matched values found by preg_replace_callback.
   *
   * @return string
   *   A string of drupal-media tag.
   */
  public static function getDrupalMediaTag(array $matches) {
    $match1 = self::convertDataAttributes($matches[1]);
    $match2 = self::convertDataAttributes($matches[2]);
    return "<drupal-media${match1} data-entity-type=\"media\"${match2}></drupal-media>";
  }

  /**
   * Cleans up all unused data attributes.
   *
   * @param string $value
   *   A substring of drupal-entity tag.
   *
   * @return string
   *   A substring without unused data attributes.
   */
  public static function convertDataAttributes(string $value) : string {
    $pattern = [
      '/ data-embed-button="[^"]+"/',
      '/ data-entity-embed-display-settings="[^"]+"/',
      // @todo The pattern below covers empty and could probably be included in
      //   the pattern above.
      '/ data-entity-embed-display-settings="*"/',
      '/ title="[^"]+"/',
    ];
    $cleaned = preg_replace($pattern, '', $value);
    return static::convertViewMode($cleaned);
  }

  /**
   * Converts view mode attributes of drupal-entity to drupal-media.
   *
   * @param string $value
   *   A substring of drupal-entity tag.
   *
   * @return string
   *   A substring with converted view mode attribute.
   */
  public static function convertViewMode(string $value) : string {
    $replaced = preg_replace('/data-entity-embed-display="/', 'data-view-mode="', $value);
    $replaced = preg_replace('/view_mode:media\./', '', $replaced);
    return $replaced;
  }

  /**
   * FieldQueueWorker constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    QueueFactory $queueFactory,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->queueFactory = $queueFactory;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('queue'),
      $container->get('entity_type.manager')
    );
  }

}
