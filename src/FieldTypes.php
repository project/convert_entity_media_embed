<?php

namespace Drupal\convert_entity_media_embed;

/**
 * Class FieldTypes.
 *
 * Define which field types the module handles.
 *
 * @package Drupal\convert_entity_media_embed
 */
class FieldTypes {

  const TYPES = [
    'text',
    'text_with_summary',
    'text_long',
  ];

}
