Convert Entity Embed to Media Embed
===================================
This module converts [entity embed](https://drupal.org/project/entity_embed) codes
to core media embed codes (and nothing else).

Since Drupal 8.8, [it has been possible](https://www.drupal.org/node/3087775)
to embed core media items in rich text fields using the *Media embed* input
filter. Before that, the entity embed module was the go-to solution for this
use-case. While the entity embed module still has its uses (the core use-case is
limited to media entities, while entity embed can handle any entity), many sites
can now fill their needs with the core solution.

Since it is always a good idea to reduce the number of moving parts in
any system, if your needs are addressed by the core mechanism, and if you don't
foresee actually needing the power of entity embed module any time soon, it
could be a good idea to remove it. However, in order to be able to do that, it
is necessary to convert the old embed tags from the entity embed module to drupal
core tags. This is where this module comes in.

How it works
------------
Upon installation, this module will look for text fields and scan for eligible
entity embed tags (only those used to embed media items) and replace them with
media embed tags. It will do the actual processing in a cron queue, so that the
potentially time-consuming process of saving all content in a large site will
not cause excessive downtime.

This principle means that site builders can enable the module in their
configuration and trust that their content will be updated without human
intervention. The module can be disabled again once it has done its work.

Progress is reported in the site's Status Report (Admin > Reports > Status
Report). When the process has not completed, it will be shown in the "Warning"
section. When it is complete, this will be reported in the "Checked" section.

**Important**: as said, the module uses a cron queue to do the heavy lifting.
This means that running cron is required to complete the conversion (multiple
cron runs may be required to complete the process). Especially on development
systems, it is common that cron does not run automatically (your production
system really should be running cron at regular intervals, at the very least
through the Automated Cron module that comes with Drupal core). Conveniently,
there is a link to run cron at the top of the Status Report to run cron, or you
could use [drush](https://www.drush.org).

What it doesn't do
------------------
The module doesn't change any of your site's configuration. It is up to you to
manage the transition from entity embed to media embed, by changing editor and
input format configuration. There are roughly two options:

1. Make all required changes at once.
2. Continue to support entity embed while not all content has been converted.

In both cases, it is recommended to remove the option to create new content
using entity embed in the same release as the conversion offered by this module
is applied. Also, note that, regardless of whether you choose to uninstall the
entity embed module in the same release as the one you use to trigger the
conversion, in general you will not be able to *remove* the entity embed module
in that same release. This is because Drupal needs a module present to be able to
uninstall it.

The choice that is left is whether to keep support for entity embed in your
input format while the conversion is taking place. Note that this means you will
need an extra release in order to uninstall entity embed (see above). In all
likelihood, this choice will depend on the amount of content that needs to be
converted and thus how long unconverted content would go without showing its
embedded media.

Should you consider making uninstalling the entity embed module a manual action,
bear in mind that you would also first need to make sure your input formats do
not depend on it before uninstalling. If your regular release process involves
Drupal configuration management, it is highly recommended to use that, since
this is not a trivial change.

Before you begin
----------------
*   **Have a backup of your database** before enabling the module.
*   Make sure you test thoroughly on a dev or test environment before running
    the module on a production system (again, make sure cron runs).
*   Make sure it is not possible to embed media entities using the old entity
    embed filter after enabling the module (e.g. by disabling the relevant
    editor button); the module will *only* convert the entities it finds upon
    installation. (Note that it is perfectly OK to uninstall and then
    re-enable the module to collect any "strays", if this does go wrong. The
    module will only update and save entities where entity embed tags are
    found).
