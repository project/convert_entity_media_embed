<?php

use Drupal\Core\Url;
use Drupal\convert_entity_media_embed\FieldTypes;

/**
 * @file
 * Install, update and uninstall functions for the Convert Entity Embed to
 *   Media Embed module.
 */

/**
 * Implements hook_install().
 */
function convert_entity_media_embed_install() {
  // Collect bundles that have any of these fields for all entity types. Build
  // an array keyed by entity type and with values of associative arrays where
  // both the key and the value are bundles names containing text and
  // text_with_summary fields.
  $bundlesPerEntityType = [];

  /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $fieldManager */
  $fieldManager = \Drupal::service('entity_field.manager');
  $fieldMap = $fieldManager->getFieldMap();

  /** @var \Drupal\field\Entity\FieldStorageConfig $field */
  foreach ($fieldMap as $entityType => $entityTypeFieldMap) {
    $bundlesPerEntityType[$entityType] = [];
    foreach ($entityTypeFieldMap as $fieldName => $fieldInfo) {
      if (!in_array($fieldInfo['type'], FieldTypes::TYPES)) {
        continue;
      }

      $bundlesPerEntityType[$entityType] += $fieldInfo['bundles'];
    }
  }

  // Build a queue of entities we need to convert. Keep track of a total number
  // of entities so we can report a percentage in the status overview.
  $totalEntities = 0;
  /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
  $queueFactory = \Drupal::service('queue');
  $queue = $queueFactory->get('convert_entity_media_embed_entities');

  $entityTypeManager = \Drupal::entityTypeManager();
  foreach ($bundlesPerEntityType as $entityTypeId => $bundles) {
    if (empty($bundles) || empty($entityTypeId)) {
      continue;
    }
    $entityType = $entityTypeManager->getDefinition($entityTypeId);
    $bundleKey = $entityType->getKey('bundle');
    $storage = $entityTypeManager->getStorage($entityTypeId);

    $query = $storage->getQuery()->accessCheck(FALSE);
    if ($bundleKey) {
      $query->condition($bundleKey, $bundles, 'IN');
    }

    $result = $query->execute();
    $totalEntities += count($result);
    foreach ($result as $entityId) {
      $queue->createItem(['entity_type' => $entityTypeId, 'id' => $entityId]);
    }
  }

  /** @var \Drupal\Core\State\StateInterface $state */
  $state = \Drupal::service('state');
  $state->set('convert_entity_media_embed_total', $totalEntities);

  \Drupal::messenger()->addStatus(t(
    'For a progress report of the conversion of entity embed tags to media embed tags, check the <a href=":status">status overview</a>.',
    [':status' => Url::fromRoute('system.status')->toString()]));
}

/**
 * Implements hook_uninstall().
 */
function convert_entity_media_embed_uninstall() {
  /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
  $queueFactory = \Drupal::service('queue');
  $entityQueue = $queueFactory->get('convert_entity_media_embed_entities');
  $entityQueue->deleteQueue();

  $state = \Drupal::service('state');
  $state->delete('convert_entity_media_embed_total');
}

/**
 * Implements hook_requirements().
 */
function convert_entity_media_embed_requirements($phase) {
  $requirements = [];

  if ($phase != 'runtime') {
    // Nothing to see here.
    return [];
  }

  $total = \Drupal::state()->get('convert_entity_media_embed_total');
  $queue = \Drupal::queue('convert_entity_media_embed_entities');
  $entitiesLeft = $queue->numberOfItems();

  $progress = $total - $entitiesLeft;
  $requirement = [
    'title' => t('Convert Entity Embed to Media Embed progress'),
  ];

  if ($progress < $total) {
    $requirement['value'] = t(
      '@progress of @total (@percentage %) of eligible entities converted.',
      [
        '@progress' => $progress,
        '@total' => $total,
        '@percentage' => intval($progress/$total * 100),
      ]);
    $requirement['severity'] = REQUIREMENT_WARNING;
  }
  else {
    $requirement['value'] = t(
      'All eligible entities converted. Please <a href=":uninstall">uninstall</a> the module.',
      [':uninstall' => Url::fromRoute('system.modules_uninstall')->toString()]
    );
    $requirement['severity'] = REQUIREMENT_INFO;
  }

  $requirements['convert_entity_media_embed_status'] = $requirement;

  return $requirements;
}
